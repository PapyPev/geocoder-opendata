Geocoder - OpenData

Utilisation de l'API adresses.data.gouv.fr pour geocoder un ensemble d'etablissements contenus dans un CSV.
Le script utilise la librairie `arcpy` d'ESRI et une connexion internet par `urllib` et `urllib2`.
