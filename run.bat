@echo off
REM
REM Chemin vers l'executable python
SET PYTHON_PATH=C:\Python27\ArcGIS10.3\python.exe
REM
REM Chemin vers le main python
SET MAIN=%~dp0\main.py
REM
REM Execution de l'outil
%PYTHON_PATH% %MAIN%
REM
PAUSE