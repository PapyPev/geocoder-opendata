#coding: utf-8

###############################################################################
# MODULES
###############################################################################


try:
    print("Chargement des modules")
    import os
    import csv
    import sys
    import json
    import arcpy
    import urllib2
    import urllib
    import urlparse
    import datetime
    import traceback
except ImportError as e:
    print(e)


###############################################################################
# CONSTANTES
###############################################################################


# Chemins
path_root = "{}".format(os.getcwd())

# Fichiers
file_conf = "{}{}{}{}{}".format(
    path_root, os.sep, "conf", os.sep, "config.json"
)
file_gdb = "{}{}{}{}{}".format(
    path_root, os.sep, "tmp", os.sep, "resultats.gdb"
)

# Decallage pour le log
decal = "  "

# Delimiter CSV
delimiter = ";"

# Systeme de coordonnées de sortie
sys_co = 27572

# Score minimal geocoder
score_min = 0.20

# Affichage modulo
modulo = 500


###############################################################################
# GLOBALES
###############################################################################


# Fichiers de donnees a geocoder
file_data = []

# Initialisation des champs
# Attention, ces champs sont ecrit en brut dans le calcul de l'adresse :
# arcpy.CalculateField_management
csv_fields = [
    "SU_ND",
    "TO_ID",
    "CODE_POSTAL",
    "TO_LABEL",
    "SU_STREET_NUMBER",
    "ST_LABEL",
    "DR_ID"
]

###############################################################################
# TOOLS
###############################################################################


def url_fix(s, charset='utf-8'):
    """Sometimes you get an URL by a user that just isn't a real
    URL because it contains unsafe characters like ' ' and so on.  This
    function can fix some of the problems in a similar way browsers
    handle data entered by the user:

    >>> url_fix(u'http://de.wikipedia.org/wiki/Elf (Begriffsklärung)')
    'http://de.wikipedia.org/wiki/Elf%20%28Begriffskl%C3%A4rung%29'

    :param charset: The target charset for the URL if the url was
                    given as unicode string.
    """
    if isinstance(s, unicode):
        s = s.encode(charset, 'ignore')
    scheme, netloc, path, qs, anchor = urlparse.urlsplit(s)
    path = urllib.quote(path, '/%')
    qs = urllib.quote_plus(qs, ':&=')
    return urlparse.urlunsplit((scheme, netloc, path, qs, anchor))


###############################################################################
# FONCTIONS
###############################################################################


def create_feature(table):
    print("{}Conversion point {}_geo".format(decal, os.path.basename(table)))

    # Variable de retour
    messages = []

    # Initialisation des destination
    layer = "{}{}{}_lyr".format(
        file_gdb,
        os.sep,
        os.path.basename(table)
    )
    feature_name = "{}_geo".format(os.path.basename(table))

    try:

        # Definition du systeme de sortie
        arcpy.env.outputCoordinateSystem = arcpy.SpatialReference(sys_co)
        # arcpy.env.outputCoordinateSystem = arcpy.SpatialReference(4326)

        # Converstion de la table en Feature Layer
        arcpy.MakeXYEventLayer_management(
            table=table,
            in_x_field="POINT_X",
            in_y_field="POINT_Y",
            out_layer=layer
        )

        # Passage de Layer a Feature
        arcpy.FeatureClassToFeatureClass_conversion(
            in_features=layer,
            out_path=file_gdb,
            out_name=feature_name
        )

    except arcpy.ExecuteError:
        msgs = arcpy.GetMessages(2)
        messages.append(msgs)
    except:
        tb = sys.exc_info()[2]
        tbinfo = traceback.format_tb(tb)[0]
        pymsg = "{}{}{}{}".format(
            "PYTHON ERRORS:\nTraceback info:\n",
            tbinfo,
            "\nError Info:\n",
            str(sys.exc_info()[1])
        )
        msgs = "ArcPy ERRORS:\n{}\n".format(arcpy.GetMessages(2))
        messages.append(pymsg)
        messages.append(msgs)

    return messages


# -----------------------------------------------------------------------------


def get_coordinates(table):
    print("{}Recuperation coords {}".format(decal, os.path.basename(table)))

    # Variable de retour
    messages = []

    try:

        # Champs adresse
        address_field = "ADRESSE_COMPLETE"

        # Ajout du champ qui contiendra l'adresse complete
        arcpy.AddField_management(
            in_table=table,
            field_name=address_field,
            field_type="TEXT"
        )

        # Ajout de deux champs contenant les coordonnées
        arcpy.AddField_management(
            in_table=table,
            field_name="POINT_X",
            field_type="FLOAT"
        )

        arcpy.AddField_management(
            in_table=table,
            field_name="POINT_Y",
            field_type="FLOAT"
        )

        code_block = """def compute_address(num, street, cp, city):
    addr = ""
    if num:
        addr += "{} ".format(num)
    if street:
        addr += "{} ".format(street)
    if cp:
        addr += "{} ".format(cp)
    if city:
        addr += "{}".format(city)
    addr = ' '.join(addr.split())
    return addr"""

        expression = "compute_address({num}, {street}, {cp}, {city})".format(
            num="!SU_STREET_NUMBER!",
            street="!ST_LABEL!",
            cp="!CODE_POSTAL!",
            city="!TO_LABEL!"
        )

        # Calcul du champs adresse complet
        arcpy.CalculateField_management(
            in_table=table,
            field=address_field,
            expression=expression,
            expression_type="PYTHON_9.3",
            code_block=code_block
        )

        # Field necessaire au prochain traitement
        fields = [address_field, "POINT_X", "POINT_Y"]

        # Creation d'un update cursor pour la table
        with arcpy.da.UpdateCursor(table, fields) as cursor:

            # Initialisation du counter
            count = 0

            # Parcours des fields
            for row in cursor:

                count += 1
                if count % modulo == 0:
                    print("{}Traitement... {}".format(decal*3, count))

                # Preparation de la requete data.gouv
                url = "{url}{adresse}".format(
                    url="http://api-adresse.data.gouv.fr/search/?q=",
                    adresse=row[0]
                )

                # Formater l'adresse pour retirer les caractères spéciaux
                url = url_fix(url)

                # Execution et récupération du contenu de la requete
                response = urllib2.urlopen(url).read()

                # Conversion en dictionnaire
                json_data = json.loads(response)

                # Recuperation des features
                json_features = json_data["features"]

                # Verification des resultats
                if json_features:

                    # Trie des résultats par score
                    json_features = sorted(
                        json_features, key=lambda k: k["properties"]["score"]
                    )

                    # Recuperation du premier element
                    result = json_features[::-1][0]

                    # Vérification du score obtenu
                    if result["properties"]["score"] > score_min:

                        # Affectation des coordonnées à la ligne (x puis y)
                        row[1] = result["geometry"]["coordinates"][0]
                        row[2] = result["geometry"]["coordinates"][1]

                        # Mise à jour de la ligne
                        cursor.updateRow(row)

                    else:
                        messages.append("Score faible [{}] pour: {}".format(
                            result["properties"]["score"], row[0]))

                else:
                    messages.append("Aucune adresse trouvée pour: {}".format(
                        row[0])
                    )

    except arcpy.ExecuteError:
        msgs = arcpy.GetMessages(2)
        messages.append(msgs)
    except:
        tb = sys.exc_info()[2]
        tbinfo = traceback.format_tb(tb)[0]
        pymsg = "{}{}{}{}".format(
            "PYTHON ERRORS:\nTraceback info:\n",
            tbinfo,
            "\nError Info:\n",
            str(sys.exc_info()[1])
        )
        msgs = "ArcPy ERRORS:\n{}\n".format(arcpy.GetMessages(2))
        messages.append(pymsg)
        messages.append(msgs)

    return messages


# -----------------------------------------------------------------------------


def create_table(filepath):

    # Variable de retour
    table = ""

    # Recuperation du nom et du futur chemin gdb
    data_table = os.path.basename(filepath).split(".")[0]
    data_table_path = "{}{}{}".format(file_gdb, os.sep, data_table)
    print("{}Creation de la table {}".format(decal, data_table))

    try:

        # Creation de la featureTable
        arcpy.CreateTable_management(
            out_path=file_gdb,
            out_name=data_table
        )

        # Ajout des champs à la gdb
        for field in csv_fields:
            arcpy.AddField_management(
                in_table=data_table_path,
                field_name=field,
                field_type="TEXT"
            )

        # Initialisation du cursor vers la nouvelle table
        cursor = arcpy.da.InsertCursor(data_table_path, csv_fields)

        # Lecture du fichier CSV
        with open(filepath) as csvfile:
            reader = csv.DictReader(csvfile, delimiter=delimiter)

            # Parcours des lignes du csv
            for row in reader:

                if not row[csv_fields[0]] == csv_fields[0]:
                    # Creation du tuple d'enregistrement des données
                    attribut_tuple = ()
                    for field in csv_fields:
                        attribut_tuple += (row[field],)

                # Enregistrement des données
                cursor.insertRow(attribut_tuple)

        del cursor

        # Alimentation de la variable de retour si tout s'est bien passé
        table = data_table_path

    except arcpy.ExecuteError:
        msgs = arcpy.GetMessages(2)
        print("{}{}".format(decal*2, msgs))
    except:
        tb = sys.exc_info()[2]
        tbinfo = traceback.format_tb(tb)[0]
        pymsg = "{}{}{}{}".format(
            "PYTHON ERRORS:\nTraceback info:\n",
            tbinfo,
            "\nError Info:\n",
            str(sys.exc_info()[1])
        )
        msgs = "ArcPy ERRORS:\n{}\n".format(arcpy.GetMessages(2))
        print("{}{}".format(decal*2, pymsg))
        print("{}{}".format(decal*2, msgs))

    return table


# -----------------------------------------------------------------------------


def do_job():

    # Variable de retour
    is_ok = True

    # Parcours des fichiers a geocoder
    print("Lecture des fichiers...")
    for filepath in file_data:

        # Verification de l'existance des chemins
        if os.path.exists(filepath):

            # Initialisation des resultats
            results = []

            # Peuplement de la table adresse
            table = create_table(filepath)
            if table:

                # Lancement du geocodage des donnees
                results = get_coordinates(table)

                # S'il y a des erreurs on affiche les erreurs
                if results:
                    for res in results:
                        print("{}{}".format(decal*2, res))

                # Converstion du resultat en classe d'entité
                results = create_feature(table)

                # S'il y a des erreurs
                if results:
                    for res in results:
                        print("{}{}".format(decal*2, res))

            else:
                is_ok = False
                print("{}Erreur creation de la table {}".format(
                    decal, os.path.basename(filepath)))

        else:
            print("{}Le chemin [{}] n'existe pas".format(decal, filepath))

    return is_ok


# -----------------------------------------------------------------------------


def is_init():
    print("Lecture du fichier de configuration...")

    # Variable globale
    global file_data

    # Variable de retour
    is_ok = True

    # Verification existence fichier de conf
    if os.path.exists(file_conf):

        # Lecture du fichier de conf
        try:

            # Lecture du fichier et sauvegarde du contenu
            with open(file_conf) as data_file:
                json_data = json.load(data_file)
                file_data = json_data["data"]

            # Initialisation de la gdb temporaire
            try:

                if arcpy.Exists(file_gdb):
                    print("{}Suppression de la gdb".format(decal))
                    arcpy.Delete_management(file_gdb)

                print("{}Creation de la gdb".format(decal))
                arcpy.CreateFileGDB_management(
                    out_folder_path=os.path.dirname(file_gdb),
                    out_name=os.path.basename(file_gdb)
                )

                # Initialisation du workspace
                arcpy.env.workspace = file_gdb

                # # Si elle n'existe pas encore
                # if not arcpy.Exists(file_gdb):

                # # Si elle existe, on supprime le contenu
                # else:

                #     # Initialisation du workspace
                #     arcpy.env.workspace = file_gdb

                #     #-- Suppression des Features
                #     features = arcpy.ListFeatureClasses()
                #     if not features is None:
                #         print("{}Nettoyage des features".format(decal))
                #         for feature in features:
                #             arcpy.DeleteFeatures_management(feature)

                #     #-- Suppression des Tables
                #     tables = arcpy.ListTables()
                #     if not tables is None:
                #         print("{}Nettoyage des tables".format(decal))
                #         for table in tables:
                #             arcpy.Delete_management(table)

            except arcpy.ExecuteError:
                is_ok = False
                msgs = arcpy.GetMessages(2)
                print("{}{}".format(decal*2, msgs))
            except:
                is_ok = False
                tb = sys.exc_info()[2]
                tbinfo = traceback.format_tb(tb)[0]
                pymsg = "{}{}{}{}".format(
                    "PYTHON ERRORS:\nTraceback info:\n",
                    tbinfo,
                    "\nError Info:\n",
                    str(sys.exc_info()[1])
                )
                msgs = "ArcPy ERRORS:\n{}\n".format(arcpy.GetMessages(2))
                print("{}{}".format(decal*2, pymsg))
                print("{}{}".format(decal*2, msgs))

        except Exception as e:
            print("{}Erreur lectur fichier de conf: {}".format(decal, e))

    else:
        is_ok = False
        print("Fichier de configuration introuvable")

    return is_ok


# -----------------------------------------------------------------------------


def main():

    date_run = datetime.datetime.now()
    if is_init():
        if do_job():
            print("SUCCESS")
        else:
            print("FAILED")
    else:
        print("FAILED")
    date_end = datetime.datetime.now()
    date_res = date_end - date_run
    print("Temps: {}s".format(date_res.total_seconds()))

if __name__ == "__main__":
    print("==================== RUN ====================")
    main()
    print("==================== END ====================")
